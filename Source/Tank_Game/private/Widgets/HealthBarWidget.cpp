// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthBarWidget.h"
#include "Components/ProgressBar.h"

UHealthBarWidget::UHealthBarWidget(const FObjectInitializer& ObjectInitializer) : UUserWidget(ObjectInitializer)
{}

void UHealthBarWidget::NativeConstruct()
{
    Super::NativeConstruct();
    if(HealthBar)
    {
        HealthBar->SetPercent(100.f);
    }
}

void UHealthBarWidget::Change_Percent(float Percent)
{
    if(HealthBar == nullptr){ return; }
    HealthBar->SetPercent(Percent);
}