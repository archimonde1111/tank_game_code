// archimonde1111


#include "HealthWidgetComponent.h"
#include "HealthBarWidget.h"

UHealthWidgetComponent::UHealthWidgetComponent()
{}

void UHealthWidgetComponent::Change_Percent(float Percent)
{
    HealthBarWidget = Cast<UHealthBarWidget>(GetUserWidgetObject());
    if(HealthBarWidget != nullptr)
    {
        HealthBarWidget->Change_Percent(Percent);
    }
}