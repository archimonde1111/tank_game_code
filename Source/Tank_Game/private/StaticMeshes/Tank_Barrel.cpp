// archimonde1111


#include "Tank_Barrel.h"
#include "Math/UnrealMathUtility.h"

void UTank_Barrel::Change_Elevation(float ElevationSpeedPercent)
{
    ElevationSpeedPercent = FMath::Clamp<float> (ElevationSpeedPercent, -1, 1);
    float ElevationSpeed = ElevationSpeedPercent * MaxElevationSpeed * GetWorld()->DeltaTimeSeconds;
    
    float NewElevation = GetRelativeRotation().Pitch + ElevationSpeed;

    NewElevation = FMath::Clamp<float> (NewElevation, MinElevation, MaxElevation);
    SetRelativeRotation(FRotator (NewElevation, 0, 0));
}