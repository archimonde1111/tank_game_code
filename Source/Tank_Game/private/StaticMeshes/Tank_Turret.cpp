// archimonde1111


#include "Tank_Turret.h"

void UTank_Turret::Rotate_Turret(float RotationSpeedPercent)
{
    RotationSpeedPercent = FMath::Clamp<float> (RotationSpeedPercent, -1, 1);
    float RotationSpeed = RotationSpeedPercent * MaxRoationSpeed * GetWorld()->DeltaTimeSeconds;
    float EndRotation = GetRelativeRotation().Yaw + RotationSpeed;

    SetRelativeRotation(FRotator(0, EndRotation, 0));
}