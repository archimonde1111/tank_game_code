//archimonde1111


#include "TankPlayerController.h"
#include "Tank_Code.h"
#include "ShooterCharacter.h"
#include "GameHUD.h"
#include "InteractableComponent.h"
#include "Tank_GameGameModeBase.h"

#include "TimerManager.h"
#include "GenericPlatform/GenericPlatformMisc.h"
#include "Kismet/GameplayStatics.h"


ATankPlayerController::ATankPlayerController()
{
    PrimaryActorTick.bCanEverTick = true;
}


void ATankPlayerController::BeginPlay()
{
    Super::BeginPlay();

    GameHUD = Cast<AGameHUD> (GetHUD());
    if(GameHUD)
    {
        GameHUD->DelHUDInitialized.AddDynamic(this, &ATankPlayerController::Display_UI);
    }
    Display_UI();
}
void ATankPlayerController::Display_UI()
{
    if(GameHUD)
    {
        if(Get_Controlled_Tank())
        {
            GameHUD->Display_Tank_UI();
            Update_Ammo_Number();
        }
        else if(Get_Controlled_Character())
        {
            GameHUD->Display_ShooterCharacter_UI();
        }
    }
}


void ATankPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    InputComponent->BindAction("Quit", IE_Pressed, this, &ATankPlayerController::Quit_Game);
    InputComponent->BindAction("Interact", IE_Pressed, this, &ATankPlayerController::Interact);
}
void ATankPlayerController::Quit_Game()//TODO Quit to menu
{
	FGenericPlatformMisc::RequestExit(false);
}
void ATankPlayerController::Interact()
{
    if(Get_Controlled_Tank() && ShooterCharacterClass)
    {
        AShooterCharacter* SpawnedCharacter = Spawn_ShooterCharacter();
        if(SpawnedCharacter)
        {
            Possess(SpawnedCharacter);
        }
    }
    else if(Get_Controlled_Character() && InteractableComponent)
    {
        InteractableComponent->Interact();
    }
}
AShooterCharacter* ATankPlayerController::Spawn_ShooterCharacter()
{
    FActorSpawnParameters SpawnParameters;
    SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
    FVector CharacterSpawnLocation = Get_Controlled_Tank()->GetActorLocation() + Get_Controlled_Tank()->GetActorRightVector() * 200;
    FRotator CharacterSpawnRotation = Get_Controlled_Tank()->GetActorRotation();
    CharacterSpawnRotation.Pitch = 0.f;
    CharacterSpawnRotation.Roll = 0.f;

    return GetWorld()->SpawnActor<AShooterCharacter> (ShooterCharacterClass, CharacterSpawnLocation, CharacterSpawnRotation, SpawnParameters);
}


void ATankPlayerController::OnPossess(APawn* InPawn)
{
    TWeakObjectPtr<AShooterCharacter> LastPossesedPawn (Cast<AShooterCharacter>(GetPawn()));
    Super::OnPossess(InPawn);

    if(InPawn)
    {
        Display_UI();

        TWeakObjectPtr<ATank_Code> PossesedTank (Cast<ATank_Code>(InPawn));
        if(PossesedTank.IsValid())
        {
            PossesedTank->TankDeath.AddUniqueDynamic(this, &ATankPlayerController::Death);
            if(LastPossesedPawn.IsValid())
            {
                LastPossesedPawn->Destroy();
            }
            return;
        }
        TWeakObjectPtr<AShooterCharacter> CharacterPossesed (Cast<AShooterCharacter>(InPawn));
        if(CharacterPossesed.IsValid())
        {
            CharacterPossesed->CharacterDeath.AddUniqueDynamic(this, &ATankPlayerController::Death);
        }
    }
}
void ATankPlayerController::OnUnPossess()
{
    TWeakObjectPtr<ATank_Code> PossesedTank (Cast<ATank_Code>(GetPawn()));
    if(PossesedTank.IsValid())
    {
        PossesedTank->TankDeath.RemoveDynamic(this, &ATankPlayerController::Death);
    }
    TWeakObjectPtr<AShooterCharacter> CharacterPossesed (Cast<AShooterCharacter>(GetPawn()));
    if(CharacterPossesed.IsValid())
    {
        CharacterPossesed->CharacterDeath.RemoveDynamic(this, &ATankPlayerController::Death);
    }
    Super::OnUnPossess();
}
void ATankPlayerController::Death()
{
    StartSpectatingOnly();
    Game_Over(false);
}
void ATankPlayerController::Game_Over(bool bPlayerWon)
{
    GameHUD->Display_EndGame_Screen(bPlayerWon);
    
    FTimerHandle TimerHandleObject;
    GetWorldTimerManager().SetTimer(TimerHandleObject, this, &APlayerController::RestartLevel, 5.f);
}


void ATankPlayerController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if(!GameHUD) { return; }

    if(Get_Controlled_Tank()) 
    { 
        Handle_Tank_Aiming();
        Check_Firing_Status();
    }
    else if(Get_Controlled_Character())
    {
        Handle_Character_Aiming();
    }
}
void ATankPlayerController::Handle_Tank_Aiming()
{
    FHitResult ObjectToAimAt;
    if(Get_Object_To_Aim_At(ObjectToAimAt))
    {
        FVector PlaceToAim = ObjectToAimAt.Location;
        Get_Controlled_Tank()->Aim_To(PlaceToAim);
    }
}
void ATankPlayerController::Handle_Character_Aiming()
{
    FHitResult ObjectToAimAt;
    if(Get_Object_To_Aim_At(ObjectToAimAt))
    {
        FVector PlaceToAim = ObjectToAimAt.Location;
        Get_Controlled_Character()->ProjectileTargetWorldLocation = PlaceToAim;

        Setup_If_Interaction_Is_Possible(ObjectToAimAt);
    }
    else
    {
        FVector2D CrosshairPosition = Get_Crosshair_Position_In_Viewport();
        FVector CameraWorldLocation;
        FVector CameraWorldDirection;
        if(Get_Camera_Location_And_Direction(CrosshairPosition, CameraWorldLocation, CameraWorldDirection))
        {
            FVector PlaceToAim = CameraWorldLocation + CameraWorldDirection * 100000;
            Get_Controlled_Character()->ProjectileTargetWorldLocation = PlaceToAim;
        }
    }
    
}
bool ATankPlayerController::Get_Object_To_Aim_At(FHitResult &OutHitResult)
{
    FVector2D CrosshairPosition = Get_Crosshair_Position_In_Viewport();
    FVector CameraWorldLocation;
    FVector CameraWorldDirection;
    if(Get_Camera_Location_And_Direction(CrosshairPosition, CameraWorldLocation, CameraWorldDirection))
    {
        return Line_Trace_From_Camera(CameraWorldLocation, CameraWorldDirection, OutHitResult);
    }
    return false;
}
FVector2D ATankPlayerController::Get_Crosshair_Position_In_Viewport()
{
    int32 SizeX, SizeY;
    GetViewportSize(SizeX, SizeY);
    FVector2D CrosshairPosition;
    CrosshairPosition.X = SizeX * CrosshairPositionX;
    CrosshairPosition.Y = SizeY * CrosshairPositionY;

    return CrosshairPosition;
}
bool ATankPlayerController::Get_Camera_Location_And_Direction(FVector2D CrosshairPosition, FVector &CameraWorldLocation, FVector &CameraWorldDirection)
{
    return DeprojectScreenPositionToWorld(CrosshairPosition.X, CrosshairPosition.Y, CameraWorldLocation, CameraWorldDirection);
}
bool ATankPlayerController::Line_Trace_From_Camera(FVector CameraWorldLocation, FVector CameraWorldDirection, FHitResult &OutHitResult)
{
    if(
        GetWorld()->LineTraceSingleByChannel
        (
            OutHitResult,
            CameraWorldLocation,
            CameraWorldLocation + CameraWorldDirection*100000,
            ECollisionChannel::ECC_Visibility
        )
      )
    {
        return true;
    }
    else
    {
        return false;
    }
}

void ATankPlayerController::Setup_If_Interaction_Is_Possible(FHitResult ObjectToAimAt)//TODO think about this name
{
    if(!Can_Player_Interact(ObjectToAimAt)) 
    { 
        InteractableComponent = nullptr;
        return; 
    }
    InteractableComponent = Get_InteractableComponent(ObjectToAimAt.GetActor());
}
bool ATankPlayerController::Can_Player_Interact(FHitResult ObjectToAimAt)
{
    return Can_Interact_With_Actor(ObjectToAimAt.GetActor()) && ObjectToAimAt.Distance < 500.f && Get_Controlled_Character();
}
bool ATankPlayerController::Can_Interact_With_Actor(AActor* HittedActor)
{
    if(!HittedActor) { return false; }

    if(HittedActor->GetComponentByClass(UInteractableComponent::StaticClass()))
    {
        return true;
    }
    return false;
}
UInteractableComponent* ATankPlayerController::Get_InteractableComponent(AActor* HittedActor)
{
    return Cast<UInteractableComponent> (HittedActor->GetComponentByClass(UInteractableComponent::StaticClass()));
}


void ATankPlayerController::Check_Firing_Status()
{
    EFiringStatus Status = Get_Controlled_Tank()->Get_FiringStatus(); //TODO Change it to observer pattern
    switch(Status)
    {
        case EFiringStatus::Aiming:
            GameHUD->Change_Crosshair_Color(FLinearColor (0, 0, 255, 1));
            break;
        case EFiringStatus::ReadyToFire:
            GameHUD->Change_Crosshair_Color(FLinearColor (0, 255, 0 ,1));
            break;
        case EFiringStatus::Reloading:
            GameHUD->Change_Crosshair_Color(FLinearColor (255, 0, 0, 1));
            Update_Ammo_Number();
            break;
        case EFiringStatus::OutOfAmmo:
            GameHUD->Change_Crosshair_Color(FLinearColor (128, 128, 128, 1));
            Update_Ammo_Number();
        default:
            break;
    }
}

ATank_Code* ATankPlayerController::Get_Controlled_Tank() const
{
    return Cast<ATank_Code> (GetPawn());
}
AShooterCharacter* ATankPlayerController::Get_Controlled_Character() const
{
    return Cast<AShooterCharacter> (GetPawn());
}

void ATankPlayerController::Update_Ammo_Number()
{
    if(GameHUD != nullptr)
	{
        int32 AmmunitionLeft = Get_Controlled_Tank()->Get_Ammo_Number();
        GameHUD->Change_AmmoDisplay(AmmunitionLeft);
	}
}
