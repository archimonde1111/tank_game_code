// archimonde1111


#include "TankAIController.h"
#include "Tank_Code.h"
#include "ShooterCharacter.h"

void ATankAIController::BeginPlay()
{
    Super::BeginPlay();
}

void ATankAIController::SetPawn(APawn* InPawn)
{
    Super::SetPawn(InPawn);
    if(InPawn)
    {
        ATank_Code* PossesedTank = Cast<ATank_Code>(InPawn);
        if(PossesedTank)
        {
            PossesedTank->TankDeath.AddUniqueDynamic(this, &ATankAIController::Tank_Death);
        }
    }
}
void ATankAIController::Tank_Death()
{
    if(!GetPawn()) { return; }

    GetPawn()->DetachFromControllerPendingDestroy();
}

void ATankAIController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if(!Get_Controlled_Tank()) { return; }
    if(!Set_Player_As_Target()) { return; }

    if(Get_Controlled_Tank()->Get_FiringStatus() == EFiringStatus::ReadyToFire)
    {
        Get_Controlled_Tank()->Fire();
    }
}
bool ATankAIController::Set_Player_As_Target()
{
    APawn* PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
    if(!PlayerPawn) { return false; }
    if(!AAIController::LineOfSightTo(PlayerPawn)) { return false; }

    MoveToActor(PlayerPawn, MinDistanceFromPlayer);
    Get_Controlled_Tank()->Aim_To(PlayerPawn->GetActorLocation());
    return true;
}

ATank_Code* ATankAIController::Get_Controlled_Tank() const
{
    return Cast<ATank_Code>(GetPawn());
}
