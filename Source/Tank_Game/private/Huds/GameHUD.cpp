// Fill out your copyright notice in the Description page of Project Settings.


#include "GameHUD.h"
#include "UIWidget.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/UMG/Public/UMG.h"


void AGameHUD::BeginPlay()
{
    Super::BeginPlay();
    
    if(TankUIWidgetClass)
    {
        TankUIWidgetObject = CreateWidget<UUIWidget> (GetWorld(), TankUIWidgetClass);
    }
    if(ShooterCharacterUIWidgetClass)
    {
        ShooterCharacterUIWidgetObject = CreateWidget<UUIWidget> (GetWorld(), ShooterCharacterUIWidgetClass);
    }
    DelHUDInitialized.Broadcast();
}

void AGameHUD::Display_Tank_UI()
{
    if(TankUIWidgetObject)
    {
        TankUIWidgetObject->AddToViewport();

        if(ShooterCharacterUIWidgetObject && ShooterCharacterUIWidgetObject->IsVisible())
        {
            ShooterCharacterUIWidgetObject->RemoveFromViewport();
        }
    }
}
void AGameHUD::Display_ShooterCharacter_UI()
{
    if(ShooterCharacterUIWidgetObject)
    {
        ShooterCharacterUIWidgetObject->AddToViewport();

        if(TankUIWidgetObject && TankUIWidgetObject->IsVisible())
        {
            TankUIWidgetObject->RemoveFromViewport();
        }
    }
}
void AGameHUD::Display_EndGame_Screen(bool bPlayerWon)
{
    if(bPlayerWon)
    {
        if(PlayerWonWidgetClass)
        {
            PlayerWonWidgetObject = CreateWidget<UUserWidget> (GetWorld(), PlayerWonWidgetClass);

            if(PlayerWonWidgetObject)
            {
                PlayerWonWidgetObject->AddToViewport();

                Remove_Widget_From_Viewport(ShooterCharacterUIWidgetObject);
                Remove_Widget_From_Viewport(TankUIWidgetObject);
            }
        }
    }
    else
    {
        if(PlayerLostWidgetClass)
        {
            PlayerLostWidgetObject = CreateWidget<UUserWidget> (GetWorld(), PlayerLostWidgetClass);

            if(PlayerLostWidgetObject)
            {
                PlayerLostWidgetObject->AddToViewport();

                Remove_Widget_From_Viewport(ShooterCharacterUIWidgetObject);
                Remove_Widget_From_Viewport(TankUIWidgetObject);
            }
        }
    }
}
void AGameHUD::Remove_Widget_From_Viewport(UUserWidget* WidgetObject)
{
    if(WidgetObject->IsVisible())
    {
        WidgetObject->RemoveFromViewport();
    }
}

void AGameHUD::Change_Crosshair_Color(FLinearColor Color)
{
    if(TankUIWidgetObject && TankUIWidgetObject->AimingDot)
    {
        TankUIWidgetObject->AimingDot->SetColorAndOpacity(Color);
    }
}
void AGameHUD::Change_AmmoDisplay(int32 Ammunition)
{
    if(TankUIWidgetObject && TankUIWidgetObject->AmmoDisplay)
    {
        TankUIWidgetObject->AmmoDisplay->SetText(FText::AsNumber(Ammunition));
    }
}