// archimonde1111


#include "MenuHUD.h"
#include "UserWidgetMenu.h"
#include "UIWidget.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/UMG/Public/UMG.h"

AMenuHUD::AMenuHUD()
{
    PrimaryActorTick.bCanEverTick = false;
}

void AMenuHUD::BeginPlay()
{
    Super::BeginPlay();

    Create_Widget();
    Add_Functionality_To_Buttons();
    Show_Menu();
}
void AMenuHUD::Create_Widget()
{
    if(MenuWidgetClass)
    {
        MenuWidgetObject = CreateWidget<UUserWidgetMenu>(GetWorld(), MenuWidgetClass);
    }
}
void AMenuHUD::Add_Functionality_To_Buttons()
{
    if(MenuWidgetObject)
    {
        if(MenuWidgetObject->StartButton)
        {
        if(!MenuWidgetObject->StartButton->OnClicked.IsBound())
            {
                MenuWidgetObject->StartButton->OnClicked.AddDynamic(this, &AMenuHUD::Start_Clicked);
            }
        }
        if(MenuWidgetObject->ExitButton)
        {
            if(!MenuWidgetObject->ExitButton->OnClicked.IsBound())
            {
                MenuWidgetObject->ExitButton->OnClicked.AddDynamic(this, &AMenuHUD::Exit_Clicked);
            }
        }
    }
}
void AMenuHUD::Show_Menu()
{
    if(MenuWidgetObject)
    {
        MenuWidgetObject->AddToViewport(100);
        GetOwningPlayerController()->bShowMouseCursor = true;
    }
}
void AMenuHUD::Start_Clicked()
{
    UGameplayStatics::OpenLevel(this, "Tank_Game");
    MenuWidgetObject->RemoveFromViewport();
}
void AMenuHUD::Exit_Clicked()
{
    FGenericPlatformMisc::RequestExit(false);
}
