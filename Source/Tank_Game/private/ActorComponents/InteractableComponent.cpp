// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableComponent.h"


UInteractableComponent::UInteractableComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


void UInteractableComponent::BeginPlay()
{
	Super::BeginPlay();

}


void UInteractableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UInteractableComponent::Interact_Implementation()
{
	UE_LOG(LogTemp, Error, TEXT("You forgot to override 'Interact' method!"));
}