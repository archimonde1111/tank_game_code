// archimonde1111

#include "TankMovementComponent.h"
#include "WheelSpawnPoint.h"
#include "Tank_Wheel.h" 
#include "DrawDebugHelpers.h"

UTankMovementComponent::UTankMovementComponent()
{
    Create_SpawnPoints();
    Attach_SpawnPoints();
}
void UTankMovementComponent::Create_SpawnPoints()
{
    for (uint16 i = 0; i < WheelsNumber; i++)
	{
		UWheelSpawnPoint* Wheel = nullptr;
		SpawnPoints.Add(Wheel);
		FString Name = FString::Printf(TEXT("SpawnPoint%i"), i+1);
		SpawnPoints[i] = CreateDefaultSubobject<UWheelSpawnPoint>(FName(Name));
	}
}
void UTankMovementComponent::Attach_SpawnPoints()
{
    for(uint16 i = 0; i < WheelsNumber; i++)
	{
        if(GetOwner() && GetOwner()->GetRootComponent())
        {
            FString name = FString::Printf(TEXT("SpawnPoint%i"), i+1);
            FName Name = FName(name);
		    SpawnPoints[i]->SetupAttachment(GetOwner()->GetRootComponent(), FName(Name));
        }
	}
}

void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
    FVector TankForwardVector = GetOwner()->GetRootComponent()->GetForwardVector();

    float PercentOfForceForward = FVector::DotProduct(MoveVelocity, TankForwardVector);
    Move_Forward(PercentOfForceForward);

    float PercentOfForceTurn = FVector::CrossProduct(TankForwardVector, MoveVelocity).Z;
    Turn_Right(PercentOfForceTurn);
}
void UTankMovementComponent::Move_Forward(float PercentOfForce)
{
    for(uint16 i = 0; i < WheelsNumber; i++)
    {
        SpawnPoints[i]->Get_Spawned_Wheel()->Move_Forward(PercentOfForce, WheelsNumber);
    }
}
void UTankMovementComponent::Turn_Right(float PercentOfForce)
{
    float WheelsNumberTurning = WheelsNumber / 2;
    for (uint16 i = 0; i < WheelsNumber; i++)
    {
        if(i < WheelsNumber / 2)
        {
            SpawnPoints[i]->Get_Spawned_Wheel()->Turn_Right(-PercentOfForce, WheelsNumberTurning);
        }
        else
        {
            SpawnPoints[i]->Get_Spawned_Wheel()->Turn_Right(PercentOfForce, WheelsNumberTurning); 
        }
    }
}