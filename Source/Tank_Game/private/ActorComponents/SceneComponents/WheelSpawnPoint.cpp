// archimonde1111

#include "WheelSpawnPoint.h"
#include "Tank_Wheel.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "ConstructorHelpers.h"

UWheelSpawnPoint::UWheelSpawnPoint()
{
	PrimaryComponentTick.bCanEverTick = true;
	Find_TankWheel_Class();
}
void UWheelSpawnPoint::Find_TankWheel_Class()//TODO Check why when set only in bp class won't appear here
{
	static ConstructorHelpers::FClassFinder<ATank_Wheel> WheelClassBP(TEXT("/Game/BluePrints/BP_Tank_Wheel"));
	if(WheelClassBP.Class)
	{
		TankWheelBP = WheelClassBP.Class;
	}
}

void UWheelSpawnPoint::BeginPlay()
{
	Super::BeginPlay();	
	Spawn_Attach_Wheel();
}
void UWheelSpawnPoint::Spawn_Attach_Wheel()
{
	if(TankWheelBP)
	{
		SpawnedWheel = GetWorld()->SpawnActorDeferred<ATank_Wheel>(TankWheelBP, GetComponentTransform());
		if(SpawnedWheel)
		{ 
			SpawnedWheel->AttachToComponent(this, FAttachmentTransformRules::KeepWorldTransform);
			UGameplayStatics::FinishSpawningActor(SpawnedWheel, GetComponentTransform());
		}
	}
}

void UWheelSpawnPoint::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

ATank_Wheel* UWheelSpawnPoint::Get_Spawned_Wheel()
{
	return SpawnedWheel;
}

