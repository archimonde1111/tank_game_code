// archimonde1111


#include "TankAimingComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Tank_Turret.h"
#include "Tank_Barrel.h"


UTankAimingComponent::UTankAimingComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();
}

bool UTankAimingComponent::Aim_To(FVector HittedLocation, float ProjectileVelocity, UTank_Barrel* Barrel, UTank_Turret* Turret)
{
	if(Barrel == nullptr)
	{return false;}
	if(Turret == nullptr)
	{return false;}

	bool bHitPossible = UGameplayStatics::SuggestProjectileVelocity
	(
		this,
		OutLaunchVelocity,
		Barrel->GetSocketLocation(FName("Projectile")),
		HittedLocation,
		ProjectileVelocity,
		false,
		0.0f,
		0.0f,
		ESuggestProjVelocityTraceOption::DoNotTrace
	);

	if(bHitPossible)
	{
		Rotate_And_Elevate(OutLaunchVelocity, Barrel, Turret);
		return true;
	}
	return false;	
}
void UTankAimingComponent::Rotate_And_Elevate(FVector LaunchVelocity, UTank_Barrel* Barrel, UTank_Turret* Turret)
{
	FRotator BarrelTargetRotation = LaunchVelocity.GetSafeNormal().Rotation();
	FRotator BarrelRotation = Barrel->GetForwardVector().Rotation();
	FRotator DeltaBarrelRotation = BarrelTargetRotation - BarrelRotation;

	Barrel->Change_Elevation(DeltaBarrelRotation.Pitch);
	
	if(FMath::Abs(DeltaBarrelRotation.Yaw) < 180) // Abs wartość bezwzględna
	{
		Turret->Rotate_Turret(DeltaBarrelRotation.Yaw);
	}
	else
	{
		Turret->Rotate_Turret(-DeltaBarrelRotation.Yaw);
	}
}