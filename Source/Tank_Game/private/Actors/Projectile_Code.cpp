//archimonde1111


#include "Projectile_Code.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"

AProjectile_Code::AProjectile_Code()
{
	PrimaryActorTick.bCanEverTick = true;

	Create_Default_Components();
	Add_Attachments();
	Setup_Components_Settings();
}
void AProjectile_Code::Create_Default_Components()
{
	CollisionMesh = CreateDefaultSubobject<UStaticMeshComponent> (TEXT("CollisionMesh"));
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent> (TEXT("ProjectileMovementComponent"));
	LaunchBlast = CreateDefaultSubobject<UParticleSystemComponent> (TEXT("LaunchBlast"));
	HitBlast = CreateDefaultSubobject<UParticleSystemComponent> (TEXT("HitBlast"));
	RadialForce = CreateDefaultSubobject<URadialForceComponent> (FName("RadialForce"));
}
void AProjectile_Code::Add_Attachments()
{
	SetRootComponent(CollisionMesh);
	LaunchBlast->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	HitBlast->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	RadialForce->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}
void AProjectile_Code::Setup_Components_Settings()
{
	CollisionMesh->SetNotifyRigidBodyCollision(true);
	CollisionMesh->SetVisibility(true);
	CollisionMesh->SetNotifyRigidBodyCollision(true);

	ProjectileMovementComponent->bAutoActivate = false;

	HitBlast->bAutoActivate = false;

	RadialForce->ImpulseStrength = 50000000.f;
	RadialForce->Radius = 500.f;
}

void AProjectile_Code::BeginPlay()
{
	Super::BeginPlay();
	CollisionMesh->OnComponentHit.AddDynamic(this, &AProjectile_Code::OnHit);
}
void AProjectile_Code::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	Activate_Projectile();
	UGameplayStatics::ApplyRadialDamage(this, BaseDamage, GetActorLocation(), RadialForce->Radius, UDamageType::StaticClass(), TArray<AActor*>());
	Initiate_SelfDestruction();
}
void AProjectile_Code::Activate_Projectile()
{
	HitBlast->Activate(true);
	SetRootComponent(HitBlast);
	CollisionMesh->DestroyComponent();
	RadialForce->FireImpulse();
}
void AProjectile_Code::Initiate_SelfDestruction()
{
	FTimerHandle TimerHandler;
	GetWorld()->GetTimerManager().SetTimer(TimerHandler, this, &AProjectile_Code::Destroy_Actor, 1.5f);
}
void AProjectile_Code::Destroy_Actor()
{
	Destroy();
}


void AProjectile_Code::Set_Projectile_Velocity(float ProjectileVelocity)
{
	ProjectileMovementComponent->SetVelocityInLocalSpace(FVector(1, 0, 0) * ProjectileVelocity);
	ProjectileMovementComponent->Activate();
}