// Fill out your copyright notice in the Description page of Project Settings.


#include "GunBase.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GunProjectile.h"


AGunBase::AGunBase()
{
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent> (FName("Root"));
	SetRootComponent(Root);
	GunMesh = CreateDefaultSubobject<USkeletalMeshComponent> (FName("GunMesh"));
	GunMesh->SetupAttachment(Root);
	GunMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AGunBase::BeginPlay()
{
	Super::BeginPlay();
}

void AGunBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void AGunBase::Fire(FVector ProjectileTargetWorldLocation)
{
	if(!GunProjectileClass) { return; }
	if(!GunMesh) { return; }

	FRotator ProjectileRotation = Calculate_Projectile_Target_Rotation(ProjectileTargetWorldLocation);
	
	UGameplayStatics::SpawnSoundAttached(FireSound, GunMesh, FName("MuzzleFlashSocket"));
	UGameplayStatics::SpawnEmitterAttached(FireFlash, GunMesh, FName("MuzzleFlashSocket"));
	
	AGunProjectile* ProjectileInstance = Spawn_Projectile(ProjectileRotation);
	ProjectileInstance->Set_Projectile_Velocity(3000.f);
}
FRotator AGunBase::Calculate_Projectile_Target_Rotation(FVector ProjectileTargetWorldLocation)
{
	FVector TargetDirection = ProjectileTargetWorldLocation - GunMesh->GetSocketLocation(FName("ProjectileSpawnPoint"));
	TargetDirection.Normalize();
	return TargetDirection.Rotation();
}
AGunProjectile* AGunBase::Spawn_Projectile(FRotator ProjectileRotation)
{
	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.Owner = GetOwner();
	
	AGunProjectile* ProjectileInstance = GetWorld()->SpawnActor<AGunProjectile> 
	(
		GunProjectileClass, 
		GunMesh->GetSocketLocation(FName("ProjectileSpawnPoint")), 
		ProjectileRotation, 
		ActorSpawnParameters
	);
	return ProjectileInstance;
}
