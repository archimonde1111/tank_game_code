// archimonde1111


#include "Mortar_Code.h"
#include "Tank_Turret.h"
#include "Tank_Barrel.h"
#include "TankTrack.h"
#include "TankMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "ConstructorHelpers.h"

AMortar_Code::AMortar_Code()
{
    PrimaryActorTick.bCanEverTick = true;//DO NOT TURN OFF UNLESS YOU WANT TO STOP MORTAR FROM SHOTTING

    Destroy_Unnecessary_Components();
    Set_Static_Meshes();
    Add_Attachments();
}
void AMortar_Code::Destroy_Unnecessary_Components()
{
    if(!(MovementComponent && LeftTrack && RightTrack)) { return; }
    MovementComponent->DestroyComponent();
    LeftTrack->DestroyComponent();
    RightTrack->DestroyComponent();
}
void AMortar_Code::Set_Static_Meshes()
{
    static ConstructorHelpers::FObjectFinder<UStaticMesh> BodyAsset(TEXT("/Game/FirstParts/Mortar/mortar_body"));
    if(BodyAsset.Object)
    {
        Body->SetStaticMesh(BodyAsset.Object);
    }
    static ConstructorHelpers::FObjectFinder<UStaticMesh> TurretAsset(TEXT("/Game/FirstParts/Mortar/mortar_dome"));
    if(TurretAsset.Object)
    {
        Turret->SetStaticMesh(TurretAsset.Object);
    }
    static ConstructorHelpers::FObjectFinder<UStaticMesh> BarrelAsset(TEXT("/Game/FirstParts/Mortar/mortar_barrel"));
    if(BarrelAsset.Object)
    {
        Barrel->SetStaticMesh(BarrelAsset.Object);
    }
}
void AMortar_Code::Add_Attachments()
{
    if(!(Turret && Barrel)) { return; }
    Turret->SetupAttachment(Body, TEXT("Doom"));
    Barrel->SetupAttachment(Turret, TEXT("Barrel"));
}
