// archimonde1111

#include "Tank_Code.h"
#include "Tank_Turret.h"
#include "Tank_Barrel.h"
#include "TankTrack.h"
#include "TankAimingComponent.h"
#include "Projectile_Code.h"
#include "TankMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "HealthWidgetComponent.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "ConstructorHelpers.h"
#include "Components/SceneCaptureComponent2D.h"


ATank_Code::ATank_Code()
{
	PrimaryActorTick.bCanEverTick = true;
	
	Setup_Tank_Mesh();
	Setup_Tank_Attachments();
	Setup_Camera();
	Add_Tank_Components();
	Setup_MiniMap_Components();
}
void ATank_Code::Setup_Tank_Mesh()
{
	Body = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tank"));
		Body->SetSimulatePhysics(true);
		Body->SetCollisionProfileName("PhysicsActor");
		static ConstructorHelpers::FObjectFinder<UStaticMesh> BodyAsset(TEXT("/Game/FirstParts/Tank/tank_fbx_Body"));
		if(BodyAsset.Object)
		{
			Body->SetStaticMesh(BodyAsset.Object);
		}
	Turret = CreateDefaultSubobject<UTank_Turret>(TEXT("Turret"));
		static ConstructorHelpers::FObjectFinder<UStaticMesh> TurretAsset(TEXT("/Game/FirstParts/Tank/tank_fbx_Turret"));
		if(TurretAsset.Object)
		{
			Turret->SetStaticMesh(TurretAsset.Object);
		}
	Barrel = CreateDefaultSubobject<UTank_Barrel>(TEXT("Barrel"));
		static ConstructorHelpers::FObjectFinder<UStaticMesh> BarrelAsset(TEXT("/Game/FirstParts/Tank/tank_fbx_Barrel"));
		if(BarrelAsset.Object)
		{
			Barrel->SetStaticMesh(BarrelAsset.Object);
		}
	LeftTrack = CreateDefaultSubobject<UTankTrack>(TEXT("LeftTrack"));
	RightTrack = CreateDefaultSubobject<UTankTrack>(TEXT("RightTrack"));
		static ConstructorHelpers::FObjectFinder<UStaticMesh> TrackAsset(TEXT("/Game/FirstParts/Tank/tank_fbx_Track"));
		if(TrackAsset.Object)
		{
			LeftTrack->SetStaticMesh(TrackAsset.Object);
			RightTrack->SetStaticMesh(TrackAsset.Object);
		}
}
void ATank_Code::Setup_Tank_Attachments()
{
	RootComponent = Body;
	Turret->SetupAttachment(Body, TEXT("Turret"));
	Barrel->SetupAttachment(Turret, TEXT("Gun"));
	LeftTrack->SetupAttachment(Body, TEXT("Track_Left"));
	RightTrack->SetupAttachment(Body, TEXT("Track_Right"));
}
void ATank_Code::Setup_Camera()
{
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	SpringArm->SetupAttachment(Body, TEXT("Camera"));
	SpringArm->TargetArmLength = 800.f;
	FRotator a(0);
	Mouse = a;
	SpringArm->SetWorldRotation(Mouse);
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
}
void ATank_Code::Add_Tank_Components()
{
	AimingComponent = CreateDefaultSubobject<UTankAimingComponent> (FName("Aiming Component"));
	MovementComponent = CreateDefaultSubobject<UTankMovementComponent> (FName("Movement Component"));
	HealthBar = CreateDefaultSubobject<UHealthWidgetComponent> (FName("HealthBar"));
	HealthBar->SetupAttachment(Body);
}
void ATank_Code::Setup_MiniMap_Components()
{
	SpringArmCaptureComp = CreateDefaultSubobject<USpringArmComponent> (FName("SpringArmCaptureComp"));
	SpringArmCaptureComp->SetupAttachment(Body);
	CaptureComponent2D = CreateDefaultSubobject<USceneCaptureComponent2D> (FName("SceneCaptureComponent2D"));
	CaptureComponent2D->SetupAttachment(SpringArmCaptureComp);
	CaptureComponent2D->ProjectionType = ECameraProjectionMode::Orthographic;
	CaptureComponent2D->OrthoWidth = 4096.f;
}

void ATank_Code::BeginPlay()
{
	Super::BeginPlay();
	if(Body)
	{
		Body->SetMassOverrideInKg(TEXT("Tank"), 50000.f, true);//must be here not in spawn tank becouse when packaging project will be problem with GEngine (Not initialized yet)
	}
	TankCurrentHealth = Health;//sets hp value if in BP was changed
}

void ATank_Code::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimeSinceLastProjectileFired += DeltaTime;

	Check_FiringStatus();
}
void ATank_Code::Check_FiringStatus()
{
	if(Amunition <= 0)
	{
		Firingstatus = EFiringStatus::OutOfAmmo;
	}
	else if(Is_Reloading())
	{
		Firingstatus = EFiringStatus::Reloading;
	}
	else if(Is_Barrel_Moving())
	{
		Firingstatus = EFiringStatus::Aiming;
	}
	else
	{
		Firingstatus = EFiringStatus::ReadyToFire;
	}
}
bool ATank_Code::Is_Reloading()
{
	if(TimeSinceLastProjectileFired >= ReloadingTime)
	{
		return false;
	}
	return true;
}
bool ATank_Code::Is_Barrel_Moving()
{
	if(!(AimingComponent || Barrel)) { return true; }

    FVector BarrelForwardVec = Barrel->GetForwardVector().GetSafeNormal();
    FVector LaunchVelocity = AimingComponent->OutLaunchVelocity.GetSafeNormal();
    if(LaunchVelocity.Equals(BarrelForwardVec, 0.05f))
    {
        return false;
    }
    return true;
}


void ATank_Code::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	Bind_Camera_Axis(PlayerInputComponent);
	Bind_Movement_Axis(PlayerInputComponent);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ATank_Code::Fire);
}
void ATank_Code::Bind_Camera_Axis(UInputComponent* PlayerInputComponent)
{
	PlayerInputComponent->BindAxis("AimAzimuth", this, &ATank_Code::Rotate_Camera_Around_Z_Axis);
	PlayerInputComponent->BindAxis("AimElevation", this, &ATank_Code::Rotate_Camera_Around_Y_Axis);
	PlayerInputComponent->BindAxis("ArmLength", this, &ATank_Code::Change_Arm_Length);
}
void ATank_Code::Rotate_Camera_Around_Z_Axis(float axis)
{
	float Azimuth = axis;
	Mouse.Yaw += Azimuth;
	SpringArm->SetRelativeRotation(Mouse);
}
void ATank_Code::Rotate_Camera_Around_Y_Axis(float axis)
{
	FRotator TurretRotation = Turret->GetComponentRotation();
	
	float Elevation = axis;
	if(Mouse.Pitch + Elevation < -90.f + TurretRotation.Pitch || Mouse.Pitch + Elevation > 20 + TurretRotation.Pitch)//TODO Name it properly
	{}
	else
	{
		Mouse.Pitch += Elevation;
		SpringArm->SetRelativeRotation(Mouse);
	}
}
void ATank_Code::Change_Arm_Length(float axis)
{
	float Length = axis;
	if(SpringArm->TargetArmLength - axis > 1500 || SpringArm->TargetArmLength - axis < 0)//TODO Name it properly
	{}
	else
	{
		SpringArm->TargetArmLength -= axis;
	}
}

void ATank_Code::Bind_Movement_Axis(UInputComponent* PlayerInputComponent)
{
	PlayerInputComponent->BindAxis("Forward", this, &ATank_Code::Move_Forward);
	PlayerInputComponent->BindAxis("Back", this, &ATank_Code::Move_Forward);
	PlayerInputComponent->BindAxis("TurnLeft", this, &ATank_Code::Turn_Right);
	PlayerInputComponent->BindAxis("TurnRight", this, &ATank_Code::Turn_Right);
}
void ATank_Code::Move_Forward(float Axis)
{
	MovementComponent->Move_Forward(Axis);
}
void ATank_Code::Turn_Right(float Axis)
{
	MovementComponent->Turn_Right(Axis);
}


void ATank_Code::Fire()
{
	if(!ProjectileBP) {return;}

	if(Firingstatus == EFiringStatus::ReadyToFire || Firingstatus == EFiringStatus::Aiming)
	{
		AProjectile_Code* Projectile = Spawn_Projectile();
			
		if(!Projectile) { return; }
		Projectile->Set_Projectile_Velocity(ProjectileVelocity);

		TimeSinceLastProjectileFired = 0;
		Amunition -= 1;
	}

}
AProjectile_Code* ATank_Code::Spawn_Projectile()
{
	AProjectile_Code* Projectile =
	GetWorld()->SpawnActor<AProjectile_Code> 
	(
		ProjectileBP, 
		Barrel->GetSocketLocation(TEXT("Projectile")),
		Barrel->GetSocketRotation(TEXT("Projectile"))
	);
	
	return Projectile;
}


float ATank_Code::TakeDamage(float DamageAmount, struct FDamageEvent const &DamageEvent, AController *EventInstigator, AActor *DamageCauser)
{
	if(HealthBar == nullptr){ return 0.f; }

	int32 DamageDealt = (int32)DamageAmount;
	DamageDealt = FMath::Clamp<int32> (DamageDealt, 0, TankCurrentHealth);

	TankCurrentHealth -= DamageDealt;
	HealthBar->Change_Percent(Get_Health_Percentage());

	if(TankCurrentHealth <= 0)
	{
		TankDeath.Broadcast();
		HealthBar->bHiddenInGame = true;
	}
	return TankCurrentHealth;
}
float ATank_Code::Get_Health_Percentage()
{
	return (float)TankCurrentHealth / (float)Health;
}

void ATank_Code::Aim_To(FVector HittedPlace)
{
	if(Barrel == nullptr) { return; }
	if(Turret == nullptr) { return; }

	AimingComponent->Aim_To(HittedPlace, ProjectileVelocity, Barrel, Turret);
}

int32 ATank_Code::Get_Ammo_Number()
{
	return Amunition;
}
EFiringStatus ATank_Code::Get_FiringStatus()
{
	return Firingstatus;
}