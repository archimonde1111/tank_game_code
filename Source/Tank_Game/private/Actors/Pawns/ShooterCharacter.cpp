// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Actors/GunBase.h"
#include "Components/SceneCaptureComponent2D.h"


AShooterCharacter::AShooterCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	Setup_Camera();
	Setup_MiniMap_Components();
}
void AShooterCharacter::Setup_Camera()
{
	SpringArm = CreateDefaultSubobject<USpringArmComponent> (FName("SpringArm"));
	Camera = CreateDefaultSubobject<UCameraComponent> (FName("Camera"));
	SpringArm->SetupAttachment(GetRootComponent());
	SpringArm->TargetArmLength = 400;
	SpringArm->bUsePawnControlRotation = true;
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
}
void AShooterCharacter::Setup_MiniMap_Components()
{
	SpringArmCaptureComp = CreateDefaultSubobject<USpringArmComponent> (FName("SpringArmCaptureComp"));
	SpringArmCaptureComp->SetupAttachment(GetMesh());
	CaptureComponent2D = CreateDefaultSubobject<USceneCaptureComponent2D> (FName("SceneCaptureComponent2D"));
	CaptureComponent2D->SetupAttachment(SpringArmCaptureComp);
	CaptureComponent2D->ProjectionType = ECameraProjectionMode::Orthographic;
	CaptureComponent2D->OrthoWidth = 4096.f;
}


void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	Spawn_Gun();
	Attach_Gun();

	CurrentHealth = MaxHealth;
}
void AShooterCharacter::Spawn_Gun()
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Owner = this;
	SpawnParameters.Instigator = this;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	GunObject = GetWorld()->SpawnActor<AGunBase> (GunClass, SpawnParameters);
}
void AShooterCharacter::Attach_Gun()
{
	if(!GunObject) { return; }

	GunObject->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("weapon_r"));
	GetMesh()->HideBoneByName(FName("weapon_r"), EPhysBodyOp::PBO_None);
}


float AShooterCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const &DamageEvent, AController *EventInstigator, AActor *DamageCauser)
{
	uint32 DamageDealt = (uint32)DamageAmount;
	DamageDealt = FMath::Clamp<uint32> (DamageDealt, 0, CurrentHealth);
	CurrentHealth -= DamageDealt;

	if(Is_Dead())
	{
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		CharacterDeath.Broadcast();
	}
	return CurrentHealth;
}

void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("AimElevation"), this, &AShooterCharacter::Look_Up);
	PlayerInputComponent->BindAxis(TEXT("AimAzimuth"), this, &AShooterCharacter::Look_Right);

	PlayerInputComponent->BindAxis(TEXT("Forward"), this, &AShooterCharacter::Move_Forward);
	PlayerInputComponent->BindAxis(TEXT("Back"), this, &AShooterCharacter::Move_Forward);
	PlayerInputComponent->BindAxis(TEXT("TurnRight"), this, &AShooterCharacter::Turn_Right);
	PlayerInputComponent->BindAxis(TEXT("TurnLeft"), this, &AShooterCharacter::Turn_Right);


	PlayerInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Trigger_Pulled);
}
void AShooterCharacter::Look_Up(float Axis)
{
	AddControllerPitchInput(-Axis);
}
void AShooterCharacter::Look_Right(float Axis)
{
	AddControllerYawInput(Axis);
}
void AShooterCharacter::Move_Forward(float Axis)
{
	AddMovementInput(GetActorForwardVector() * Axis);
}
void AShooterCharacter::Turn_Right(float Axis)
{
	AddMovementInput(GetActorRightVector() * Axis);
}

void AShooterCharacter::Trigger_Pulled()
{
	if(!GunObject) { return; }

	GunObject->Fire(ProjectileTargetWorldLocation);
}
bool AShooterCharacter::Is_Dead()
{
	return CurrentHealth <= 0;
}
float AShooterCharacter::Get_Health_Percent()
{
	return (float)CurrentHealth/(float)MaxHealth;
}


void AShooterCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(GunObject)
	{
		GunObject->Destroy();
	}
	Super::EndPlay(EndPlayReason);
}