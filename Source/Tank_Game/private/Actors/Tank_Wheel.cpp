// archimonde1111

#include "Tank_Wheel.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Components/SphereComponent.h"

//TODO Only call methods responsible for moving when wheels touches ground
ATank_Wheel::ATank_Wheel()
{
	PrimaryActorTick.bCanEverTick = true;

	Create_Default_Components();
}
void ATank_Wheel::Create_Default_Components()
{
	Suspension = CreateDefaultSubobject<UPhysicsConstraintComponent> (FName("Suspension"));
	SuspensionSM = CreateDefaultSubobject<USphereComponent> (FName("SuspensionSM"));
	WheelAxis = CreateDefaultSubobject<UPhysicsConstraintComponent> (FName("WheelAxis"));
	Wheel = CreateDefaultSubobject<USphereComponent> (FName("Wheel"));

	SuspensionSM->SetSimulatePhysics(true);
	Wheel->SetSimulatePhysics(true);
	Setup_Attachments();

}
void ATank_Wheel::Setup_Attachments()
{
	SetRootComponent(Suspension);
	SuspensionSM->SetupAttachment(Suspension);
	WheelAxis->SetupAttachment(SuspensionSM);
	Wheel->SetupAttachment(WheelAxis);
}

void ATank_Wheel::BeginPlay()
{
	Super::BeginPlay();
	Setup_Constrains();
}
void ATank_Wheel::Setup_Constrains()
{
	if(!GetAttachParentActor()) { return; }
	UPrimitiveComponent* Body = Cast<UPrimitiveComponent>(GetAttachParentActor()->GetRootComponent());
	if(Body)
	{
		Suspension->SetConstrainedComponents(Body, NAME_None, SuspensionSM, NAME_None);
		WheelAxis->SetConstrainedComponents(SuspensionSM, NAME_None, Wheel, NAME_None);
	}
}

void ATank_Wheel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATank_Wheel::Move_Forward(float ForcePercent, uint16 WheelsNumber)
{
	ForcePercent = FMath::Clamp<float> (ForcePercent, -1, 1);
	FVector ForwardVector = GetActorForwardVector().GetSafeNormal();
	FVector ForceToApply = ForcePercent * ForwardVector * WheelsTorqueForce / WheelsNumber *1.7;//1.7 && 0.85 values created so tank will move more realistic (tested with WheelsTorqueForce = 35000000)
	Wheel->AddForce(ForceToApply);
}

void ATank_Wheel::Turn_Right(float ForcePercent, uint16 WheelsNumber)
{
	ForcePercent = FMath::Clamp<float> (ForcePercent, -1, 1);
	FVector ForwardVector = GetActorForwardVector().GetSafeNormal();
	FVector ForceToApply = ForcePercent * ForwardVector * WheelsTorqueForce / WheelsNumber *0.85;
	Wheel->AddForce(ForceToApply);
}