// Fill out your copyright notice in the Description page of Project Settings.


#include "GunProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"


AGunProjectile::AGunProjectile()
{
	PrimaryActorTick.bCanEverTick = true;

	Create_Subobjects();	
	Setup_Subobjects();
}
void AGunProjectile::Create_Subobjects()
{
	CollisionMesh = CreateDefaultSubobject<UStaticMeshComponent> (TEXT("CollisionMesh"));
	HitFlash = CreateDefaultSubobject<UParticleSystemComponent> (TEXT("HitFlash"));
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent> (TEXT("ProjectileMovementComponent"));
}
void AGunProjectile::Setup_Subobjects()
{
	SetRootComponent(CollisionMesh);
	CollisionMesh->SetNotifyRigidBodyCollision(true);

	HitFlash->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	HitFlash->bAutoActivate = false;

	ProjectileMovementComponent->bAutoActivate = false;
	ProjectileMovementComponent->ProjectileGravityScale = 0.f;
}

void AGunProjectile::BeginPlay()
{
	Super::BeginPlay();

	if(CollisionMesh)
	{
		CollisionMesh->OnComponentHit.AddUniqueDynamic(this, &AGunProjectile::OnHit);
	}
}
void AGunProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& OutHit)
{
	if(!GetOwner()) { return; }
	Activate_Projectile();
	UGameplayStatics::ApplyDamage(OtherActor, DamageDealt, GetOwner()->GetInstigatorController(), GetOwner(), UDamageType::StaticClass());
	Intiate_SelfDestruction();
}
void AGunProjectile::Activate_Projectile()
{
	if(!HitFlash) { return; }

	HitFlash->Activate(true);
	UGameplayStatics::SpawnSoundAtLocation(this, HitSound, GetActorLocation());
	SetRootComponent(HitFlash);
	CollisionMesh->DestroyComponent();
}
void AGunProjectile::Intiate_SelfDestruction()
{
	FTimerHandle TimerHandler;
	GetWorld()->GetTimerManager().SetTimer(TimerHandler, this, &AGunProjectile::Destroy_Actor, 0.5f);
}
void AGunProjectile::Destroy_Actor()
{
	Destroy();
}

void AGunProjectile::Set_Projectile_Velocity(float Velocity)
{
	if(!ProjectileMovementComponent) { return; }

	ProjectileMovementComponent->SetVelocityInLocalSpace(FVector::ForwardVector * Velocity);
	ProjectileMovementComponent->Activate();
}