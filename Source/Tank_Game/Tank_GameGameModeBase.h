// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Tank_GameGameModeBase.generated.h"

class ATriggerSphere;

/**
 * 
 */
UCLASS()
class TANK_GAME_API ATank_GameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSoftObjectPtr<ATriggerSphere> BaseOne;

};
