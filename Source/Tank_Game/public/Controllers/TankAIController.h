// archimonde1111
#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TankAIController.generated.h"

class ATank_Code;
class AShooterCharacter;

UCLASS()
class TANK_GAME_API ATankAIController : public AAIController
{
	GENERATED_BODY()

	UFUNCTION()
	void Tank_Death();

	bool Set_Player_As_Target();

public:
	void BeginPlay() override;
	void Tick(float DeltaTime) override;
	void SetPawn(APawn* InPawn) override;

protected:
	ATank_Code* Get_Controlled_Tank() const;

	float MinDistanceFromPlayer = 5000.f;
	
};
