// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

class ATank_Code;
class AShooterCharacter;
class AGameHUD;
class UInteractableComponent;

UCLASS()
class TANK_GAME_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()

	UPROPERTY()
		AGameHUD* GameHUD = nullptr;
	UPROPERTY()
		UInteractableComponent* InteractableComponent = nullptr;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AShooterCharacter> ShooterCharacterClass = nullptr;

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual void SetupInputComponent() override;
	UFUNCTION()
		void Quit_Game();
	UFUNCTION()
		void Interact();

public:
	ATankPlayerController();
	void OnPossess(APawn* InPawn) override;
	void OnUnPossess() override;

	UPROPERTY(EditAnywhere)
		float CrosshairPositionX = 0.5f;
	UPROPERTY(EditAnywhere)
		float CrosshairPositionY = 0.33333f;

	UFUNCTION(BlueprintCallable)
		void Game_Over(bool bPlayerWon);

private:
	UFUNCTION()
		ATank_Code* Get_Controlled_Tank() const;
	UFUNCTION()
		AShooterCharacter* Get_Controlled_Character() const;

	UFUNCTION()
		void Handle_Tank_Aiming();
	UFUNCTION()
		void Handle_Character_Aiming();

	UFUNCTION()
		bool Get_Object_To_Aim_At(FHitResult &OutHitResult);
	UFUNCTION()
		FVector2D Get_Crosshair_Position_In_Viewport();
	UFUNCTION()
		bool Get_Camera_Location_And_Direction(FVector2D CrosshairPosition, FVector &CameraWorldLocation, FVector &CameraWorldDirection);
	UFUNCTION()
		bool Line_Trace_From_Camera(FVector CameraWorldLocation, FVector CameraWorldDirection, FHitResult &OutHitResult);
	
	UFUNCTION()
		void Setup_If_Interaction_Is_Possible(FHitResult ObjectToAimAt);
	UFUNCTION()
		bool Can_Player_Interact(FHitResult ObjectToAimAt);
	UFUNCTION()
		bool Can_Interact_With_Actor(AActor* HittedActor);
	UFUNCTION()
		UInteractableComponent* Get_InteractableComponent(AActor* HittedActor);


	UFUNCTION()
		AShooterCharacter* Spawn_ShooterCharacter();


	UFUNCTION()
		void Check_Firing_Status();
	UFUNCTION()
		void Display_UI();
	UFUNCTION()
		void Update_Ammo_Number();
	UFUNCTION()
		void Death();
};
