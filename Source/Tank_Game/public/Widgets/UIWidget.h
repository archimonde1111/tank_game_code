// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UIWidget.generated.h"

class UCanvasPanel;
class UImage;
class UTextBlock;
class UProgressBar;

/**
 * 
 */
UCLASS()
class TANK_GAME_API UUIWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UUIWidget(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct() override;//used as BeginPlay

	UPROPERTY()
		UCanvasPanel* Canvas_Panel = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
		UProgressBar* BaseCaptureBar = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
		UImage* AimingDot = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTextBlock* AmmoDisplay = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
		UImage* MiniMap = nullptr;

};
