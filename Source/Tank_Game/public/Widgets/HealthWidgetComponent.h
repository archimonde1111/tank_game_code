// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "HealthWidgetComponent.generated.h"

class UHealthBarWidget;

/**
 * 
 */
UCLASS()
class TANK_GAME_API UHealthWidgetComponent : public UWidgetComponent
{
	GENERATED_BODY()

	UPROPERTY()
		UHealthBarWidget* HealthBarWidget = nullptr;

public:
	UHealthWidgetComponent();
	void Change_Percent(float Percent);
};
