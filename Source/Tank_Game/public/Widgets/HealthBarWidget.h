// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HealthBarWidget.generated.h"

/**
 * 
 */
UCLASS()
class TANK_GAME_API UHealthBarWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	class UCanvasPanel* CanvasPanel = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
	class UProgressBar* HealthBar = nullptr;

public:
	UHealthBarWidget(const FObjectInitializer& ObjectInitializer);

	void NativeConstruct() override;
	
	void Change_Percent(float Percent);
};
