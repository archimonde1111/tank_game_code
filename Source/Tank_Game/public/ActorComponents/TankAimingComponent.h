// archimonde1111
#pragma once

#include "CoreMinimal.h"
#include "TankAimingComponent.generated.h"

class UTank_Turret;
class UTank_Barrel;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TANK_GAME_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

	void Get_Tank();
	void Rotate_And_Elevate(FVector LaunchVelocity, UTank_Barrel* Barrel, UTank_Turret* Turret);
public:	
	UTankAimingComponent();

	FVector OutLaunchVelocity;

	bool Aim_To(FVector HittedLocation, float ProjectileVelocity, UTank_Barrel* Barrel, UTank_Turret* Turret);
protected:
	virtual void BeginPlay() override;
};
