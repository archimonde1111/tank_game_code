// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/NavMovementComponent.h"
#include "TankMovementComponent.generated.h"

class UWheelSpawnPoint;
class ATank_Wheel;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TANK_GAME_API UTankMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()
	
public:
	UTankMovementComponent();

	void Move_Forward(float PercentOfForce);
	void Turn_Right(float PercentOfForce);

private:
	void RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed) override;
	void Create_SpawnPoints();
	void Attach_SpawnPoints();

protected:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)//TODO check which one shows array's objects in BP
		TArray<UWheelSpawnPoint*> SpawnPoints;
	UPROPERTY()
		TArray<ATank_Wheel*> SpawnedWheels;
	UPROPERTY(EditDefaultsOnly)
		uint16 WheelsNumber = 12;
};
