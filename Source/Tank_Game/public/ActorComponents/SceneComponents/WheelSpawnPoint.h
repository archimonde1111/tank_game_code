// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "WheelSpawnPoint.generated.h"

class ATank_Wheel;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TANK_GAME_API UWheelSpawnPoint : public USceneComponent
{
	GENERATED_BODY()

	UPROPERTY()
		ATank_Wheel* SpawnedWheel = nullptr;

	void Find_TankWheel_Class();
	void Spawn_Attach_Wheel();
public:	
	UWheelSpawnPoint();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "SpawnBP")
		TSubclassOf<ATank_Wheel>TankWheelBP = nullptr;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	ATank_Wheel* Get_Spawned_Wheel();
		
};
