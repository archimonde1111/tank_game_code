// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tank_Wheel.generated.h"

class UPhysicsConstraintComponent;
class USphereComponent;

UCLASS()
class TANK_GAME_API ATank_Wheel : public AActor
{
	GENERATED_BODY()

	void Create_Default_Components();
	void Setup_Attachments();
	void Setup_Constrains();
	
public:	
	ATank_Wheel();

protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly)
		UPhysicsConstraintComponent* Suspension;
	UPROPERTY(EditDefaultsOnly)
		USphereComponent* SuspensionSM;
	UPROPERTY(EditDefaultsOnly)
		UPhysicsConstraintComponent* WheelAxis;
	UPROPERTY(EditDefaultsOnly)
		USphereComponent* Wheel;

	UPROPERTY(EditDefaultsOnly, Category = "TorqueForce")
		float WheelsTorqueForce = 35000 * 1000;
	
public:	
	virtual void Tick(float DeltaTime) override;

	void Move_Forward(float ForcePercent, uint16 WheelNumber);
	void Turn_Right(float ForcePercent, uint16 WheelsNUmber);

};
