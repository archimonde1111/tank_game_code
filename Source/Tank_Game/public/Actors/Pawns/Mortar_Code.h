// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "Tank_Code.h"
#include "Mortar_Code.generated.h"

/**
 * 
 */
UCLASS()
class TANK_GAME_API AMortar_Code : public ATank_Code
{
	GENERATED_BODY()

	UFUNCTION()
		void Destroy_Unnecessary_Components();
	UFUNCTION()
		void Set_Static_Meshes();
	UFUNCTION()
		void Add_Attachments(); 
	
public:
	AMortar_Code();
};
