// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Tank_Code.generated.h"

UENUM()
enum class EFiringStatus : uint8
{
	ReadyToFire,
	Aiming,
	Reloading,
	OutOfAmmo
};

class UTank_Turret;
class UTank_Barrel;
class UTankTrack;
class UTankAimingComponent;
class AProjectile_Code;
class UTankMovementComponent;
class USpringArmComponent;
class UCameraComponent;
class UHealthWidgetComponent;
class USceneCaptureComponent2D;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTank_Delegate);

UCLASS()
class TANK_GAME_API ATank_Code : public APawn
{
	GENERATED_BODY()

	void Setup_Tank();
	void Setup_Tank_Mesh();
	void Setup_Tank_Attachments();
	void Setup_Camera();
	void Add_Tank_Components();
	void Setup_MiniMap_Components();


	bool Is_Barrel_Moving();
	bool Is_Reloading();
	AProjectile_Code* Spawn_Projectile();

	void Bind_Camera_Axis(UInputComponent* PlayerInputComponent);
	void Bind_Movement_Axis(UInputComponent* PlayerInputComponent);

	UPROPERTY(EditDefaultsOnly, Category = "Projectile")
		TSubclassOf<AProjectile_Code> ProjectileBP;
	UPROPERTY(EditAnywhere, Category = "Projectile")
		int32 Amunition = 3;
	UPROPERTY(EditAnywhere, Category = "Projectile")
		float ProjectileVelocity = 5000.f;
	UPROPERTY(EditAnywhere, Category = "Projectile")
		float ReloadingTime = 7.f;
	UPROPERTY(EditAnywhere, Category = "Health")
		int32 Health = 100.f;
	UPROPERTY(VisibleAnyWhere, Category = "Health")
		int32 TankCurrentHealth = Health;

	float TimeSinceLastProjectileFired = 0.f;

	EFiringStatus Firingstatus = EFiringStatus::Aiming;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* Body = nullptr;
	UPROPERTY(EditDefaultsOnly)
		UTank_Turret* Turret = nullptr;
	UPROPERTY(EditDefaultsOnly)
		UTank_Barrel* Barrel = nullptr;
	UPROPERTY(EditDefaultsOnly)
		UTankTrack* LeftTrack = nullptr;
	UPROPERTY(EditDefaultsOnly)
		UTankTrack* RightTrack = nullptr;

	UPROPERTY(EditDefaultsOnly)
		UTankAimingComponent* AimingComponent;
	UPROPERTY()
		UTankMovementComponent* MovementComponent;

	UPROPERTY()
		USpringArmComponent* SpringArm;
	UPROPERTY()
		UCameraComponent* Camera;
	FRotator Mouse;


	UPROPERTY(EditDefaultsOnly)
		USpringArmComponent* SpringArmCaptureComp = nullptr;
	UPROPERTY(EditAnywhere)
		USceneCaptureComponent2D* CaptureComponent2D = nullptr;
	UPROPERTY(EditAnywhere, Category = "Health")
		UHealthWidgetComponent* HealthBar = nullptr;
	
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser) override;
public:
	ATank_Code();

	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void Rotate_Camera_Around_Z_Axis(float axis);
	void Rotate_Camera_Around_Y_Axis(float axis);
	void Change_Arm_Length(float axis);

	void Move_Forward(float Axis);
	void Turn_Right(float Axis);

	void Aim_To(FVector HittedPlace);
	void Fire();
	void Check_FiringStatus();
	EFiringStatus Get_FiringStatus();

	float Get_Health_Percentage();
	int32 Get_Ammo_Number();

	FTank_Delegate TankDeath;
};
