// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class USceneCaptureComponent2D;
class AGunBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FShooterCharacterDelegate);

UCLASS()
class TANK_GAME_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		uint32 MaxHealth = 100;
	UPROPERTY(VisibleAnywhere)
		uint32 CurrentHealth = 0;


public:
	AShooterCharacter();
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

protected:
	virtual void BeginPlay() override;
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const &DamageEvent, AController *EventInstigator, AActor *DamageCauser) override;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AGunBase> GunClass;
		
	UPROPERTY(EditDefaultsOnly)
		USpringArmComponent* SpringArm = nullptr;
	UPROPERTY(EditDefaultsOnly)
		UCameraComponent* Camera = nullptr;
	UPROPERTY()
		AGunBase* GunObject = nullptr;

	UPROPERTY(EditDefaultsOnly)
		USpringArmComponent* SpringArmCaptureComp = nullptr;
	UPROPERTY(EditAnywhere)
		USceneCaptureComponent2D* CaptureComponent2D = nullptr;
		

	void Setup_Camera();
	void Setup_MiniMap_Components();
	virtual void Spawn_Gun();
	virtual void Attach_Gun();

	void Trigger_Pulled();

	void Look_Up(float Axis);
	void Look_Right(float Axis);
	void Move_Forward(float Axis);
	void Turn_Right(float Axis);
public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
		bool Is_Dead();
	UFUNCTION(BlueprintCallable)
		float Get_Health_Percent();

	FVector ProjectileTargetWorldLocation;
	FShooterCharacterDelegate CharacterDeath;
};
