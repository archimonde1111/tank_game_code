// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile_Code.generated.h"

class UProjectileMovementComponent;
class UParticleSystemComponent;
class URadialForceComponent;

UCLASS()
class TANK_GAME_API AProjectile_Code : public AActor
{
	GENERATED_BODY()

	void Create_Default_Components();
	void Add_Attachments();
	void Setup_Components_Settings();

	void Activate_Projectile();
	void Initiate_SelfDestruction();
	
public:	
	AProjectile_Code();

	void Set_Projectile_Velocity(float ProjectileVelocity);
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
		UProjectileMovementComponent* ProjectileMovementComponent = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		UStaticMeshComponent* CollisionMesh = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "ParticleSys")
		UParticleSystemComponent* LaunchBlast = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "ParticleSys")
		UParticleSystemComponent* HitBlast = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "ParticleSys")
		URadialForceComponent* RadialForce = nullptr;

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		float BaseDamage = 20.f;
	
	void Destroy_Actor();
};
