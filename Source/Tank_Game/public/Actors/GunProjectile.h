// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GunProjectile.generated.h"

class UProjectileMovementComponent;

UCLASS()
class TANK_GAME_API AGunProjectile : public AActor
{
	GENERATED_BODY()

	void Destroy_Actor();
	
public:	
	AGunProjectile();

	void Set_Projectile_Velocity(float Velocity);
protected:
	virtual void BeginPlay() override;

	void Create_Subobjects();
	void Setup_Subobjects();

	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* CollisionMesh = nullptr;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystemComponent* HitFlash = nullptr;
	UPROPERTY(EditDefaultsOnly)
		USoundBase* HitSound = nullptr;

	UPROPERTY(EditDefaultsOnly)
		UProjectileMovementComponent* ProjectileMovementComponent = nullptr;


	UPROPERTY(EditDefaultsOnly)
		float DamageDealt = 10;

	UFUNCTION()
		virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& OutHit);
	virtual void Activate_Projectile();
	virtual void Intiate_SelfDestruction();
};
