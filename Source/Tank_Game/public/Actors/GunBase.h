// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GunBase.generated.h"

class USkeletalMeshComponent;
class UParticleSystem;
class AGunProjectile;

UCLASS()
class TANK_GAME_API AGunBase : public AActor
{
	GENERATED_BODY()
	
	AGunProjectile* Spawn_Projectile(FRotator ProjectileRotation);
	FRotator Calculate_Projectile_Target_Rotation(FVector ProjectileTargetWorldLocation);
public:	
	AGunBase();
	virtual void Tick(float DeltaTime) override;


	virtual void Fire(FVector ProjectileTargetWorldLocation = FVector(0));

protected:
	virtual void BeginPlay() override;


protected:
	UPROPERTY()
		USceneComponent* Root = nullptr;
	UPROPERTY(EditDefaultsOnly)
		USkeletalMeshComponent* GunMesh = nullptr;
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* FireFlash = nullptr;
	UPROPERTY(EditDefaultsOnly)
		USoundBase* FireSound = nullptr;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AGunProjectile> GunProjectileClass = nullptr;



};
