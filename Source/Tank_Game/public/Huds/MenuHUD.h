// archimonde1111

#pragma once

#include "GameFramework/HUD.h"
#include "CoreMinimal.h"
#include "MenuHUD.generated.h"

class UUserWidgetMenu;

/**
 * 
 */
UCLASS()
class TANK_GAME_API AMenuHUD : public AHUD
{
	GENERATED_BODY()
	
	UPROPERTY()
		UUserWidgetMenu* MenuWidgetObject;
public:
	AMenuHUD();

	virtual void BeginPlay() override;

	void Create_Widget();
protected:
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<UUserWidgetMenu> MenuWidgetClass = nullptr;

private:
	void Add_Functionality_To_Buttons();
	void Show_Menu();

	UFUNCTION()
	void Start_Clicked();
	UFUNCTION()
	void Exit_Clicked();

	bool bGameWidget = false;
};
