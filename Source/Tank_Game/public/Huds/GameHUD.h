// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "GameHUD.generated.h"

class UUIWidget;
class UUserWidget;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHUDInitialized);

/**
 * 
 */
UCLASS()
class TANK_GAME_API AGameHUD : public AHUD
{
	GENERATED_BODY()
	
	UPROPERTY()
		UUIWidget* TankUIWidgetObject = nullptr;
	UPROPERTY()
		UUIWidget* ShooterCharacterUIWidgetObject = nullptr;
	UPROPERTY()
		UUserWidget* PlayerWonWidgetObject = nullptr;
	UPROPERTY()
		UUserWidget* PlayerLostWidgetObject = nullptr;
	

	void Remove_Widget_From_Viewport(UUserWidget* WidgetObject);
public:
	void Change_Crosshair_Color(FLinearColor Color);
	void Change_AmmoDisplay(int32 Ammunition);

	void Display_Tank_UI();
	void Display_ShooterCharacter_UI();

	void Display_EndGame_Screen(bool bPlayerWon);

	FHUDInitialized DelHUDInitialized;
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUIWidget> TankUIWidgetClass = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUIWidget> ShooterCharacterUIWidgetClass = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUserWidget> PlayerWonWidgetClass = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUserWidget> PlayerLostWidgetClass = nullptr;
};
