// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Tank_GameGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TANK_GAME_API UTank_GameGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
};
