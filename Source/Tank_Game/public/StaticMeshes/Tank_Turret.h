// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Tank_Turret.generated.h"

/**
 * 
 */
UCLASS()
class TANK_GAME_API UTank_Turret : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	void Rotate_Turret(float RotationSpeedPercent);
	
private:
	UPROPERTY(EditAnywhere, Category = "Setup")
	float MaxRoationSpeed = 20.f;
};
