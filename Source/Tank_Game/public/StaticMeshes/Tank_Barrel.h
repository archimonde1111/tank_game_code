// archimonde1111

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Tank_Barrel.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class TANK_GAME_API UTank_Barrel : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	void Change_Elevation(float ElevationSpeedPercent);
	

private:

	UPROPERTY(EditAnywhere, Category = "Setup")
	float MaxElevationSpeed = 10.f;
	UPROPERTY(EditAnywhere, Category = "Setup")
	float MaxElevation = 40.f;
	UPROPERTY(EditAnywhere, Category = "Setup")
	float MinElevation = 0;
};
